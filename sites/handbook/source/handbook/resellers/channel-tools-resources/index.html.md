---
layout: handbook-page-toc
title: "Channel Partner Tools and Resources"
description: "Channel partner tools and resources to help grow your GitLab business."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# **Channel Partner Tools and Resources**
  
**We invite organizations interested in becoming a GitLab Channel Partner to [APPLY NOW](https://partners.gitlab.com/English/register_email.aspx).**

GitLab Channel Partners are the primary audience for this Handbook page.

## **GitLab Partner Portal**

Partners are given access to our [partner portal](https://partners.gitlab.com) when they are onboarded. This serves as the primary source of information and transactions for our partners. The GitLab Partner Portal has the following capabilities:
* Partner registration and account management
* Deal registration and opportunity management
* Training accreditation and other learning
* Sales, services, and program and marketing resources
* GitLab news, communications and program announcements.
 
Visit the portal at [partners.gitlab.com](https://partners.gitlab.com). There is a walk-through video available on the portal or you can [watch it here](https://drive.google.com/file/d/1cAGBOWrvRRL71zPPrUkwLm0L4BP_MzF_/view).
 
## **GitLab Landing Page Guidelines**

GitLab Channel Partners need a landing page with information about GitLab as part of their organization website. Here is what we look for in a webpage:

* We prefer your landing page URL be created as yourcompanyurl.com/GitLab. If this is not possible, we ask you to set a redirect for that URL to the actual one.

* We encourage your landing page to be in the local language. There are plenty of resources on GitLab, many in additional languages, however you can translate any of our Instant Marketing campaign materials (found in the marketing section of the partner portal) onto your website.

* Your organization's GitLab webpage should use the Authorized Reseller logo (available on the partner portal) and link back to us at [about.gitlab.com](https://about.gitlab.com/).
 
* Your webpage should include a “What Is GitLab?” paragraph which can be found in our [press kit](https://about.gitlab.com/press/press-kit/) along with our logo. You can find additional information in our [reseller marketing kit](https://about.gitlab.com/handbook/marketing/strategic-marketing/reseller-kit/). 
 
* To ensure you are consistently representing our product on your website, we ask that wherever you mention the GitLab product or features, there should be a link back to the corresponding item on [about.gitlab.com](https://about.gitlab.com/).

* While not required, we encourage partners to include a Free Trial button on your GitLab webpage. This Free Trial button is customized to include your unique ID and tags all leads to your organization. The 30-day Free Trial helps your customer try GitLab for one month free and gives you the opportunity to discuss how it's working for them. To learn more about this offering go to the [Marketing Demand Generation section](https://partners.gitlab.com/prm/English/c/marketing-demand-gen) of the partner portal.

## **Marketing Resources**

Channel partners have several programs and tools available to help them market and sell GitLab. The following are a few of the programs offered; additional programs are added periodically so bookmark this page to ensure you always have the latest information. 

### **GitLab Brand and Logo**

Bookmark the [GitLab Brand and Logo website](https://design.gitlab.com/brand-logo/core-logo/) to ensure you have access to these materials. 

### **Authorized Reseller Logo**
The GitLab Authorized Reseller logo allows your prospects and customers to know that GitLab is working with you.

* Use the GitLab Authorized Reseller logo on your materials where appropriate and in accordance with our [brand and logo guidelines](https://design.gitlab.com/brand-logo/core-logo/).
* The logos are available in the [README file](https://gitlab.com/gitlab-com/resellers/) of the resellers project. Also, the logos can also be downloaded from the Marketing collection of the Asset Library in the [partner portal](https://partners.gitlab.com).

### **GitLab Press Kit**

Download the [GitLab Press Kit](https://about.gitlab.com/press/press-kit/) for access to our latest "About Us" information as well as other key details. Note, a press release kit for you to use to announce your partnership with GitLab is available in the marketing section of the partner portal. 

### **Partner Instant Marketing Campaigns** 

Weʼve developed a full suite of GitLab marketing assets for partners, including ebooks, infographics, videos, digital ads, social media posts, and more. They can select what they need, customize the materials with a logo, and begin marketing. Social media materials are also available. Materials are available in 6 languages: English, French, German, Japanese, Korean and Spanish.
 
Start using these materials today by going to the [Marketing Demand Generation section](https://partners.gitlab.com/prm/English/c/marketing-demand-gen) of the partner portal.

### **Partner Microsite Program** 

Keeping your partner websites up-to-date with the latest GitLab content is easy with our partner microsite program. Partner's simply provide GitLab with their logo, give us an "About Us" statement and begin your nurture effort. Topics on the microsite include GitOps, DevOps, DevSecOps, Ultimate Upgrade and Automated Software delivery. GitLab creates and maintains these SEO-friendly, co-branded microsite, with customized URLs for partners to use to directly with customers. 
 
Learn more by going to the [Marketing Demand Generation section](https://partners.gitlab.com/prm/English/c/marketing-demand-gen) of the partner portal.

### **Customer Case Studies** 

Testimonials from happy customers are a great tool to support partners, but gathering this information can be difficult and time-consuming. GitLab is offering our partners access to UserEvidence software which can quickly and easily collect customer feedback and turn it into compelling quotes, graphics, and other publishable materials. To participate in this program, partners should contact [partner marketing](mailto:Partner-Marketing@gitlab.com) or learn more by going to the [Marketing Demand Generation section](https://partners.gitlab.com/prm/English/c/marketing-demand-gen) of the partner portal. 

### **Concierge Marketing Program**

Select partners are elibible to participate in this program. Get the help you need to expand your customer base, build brand loyalty, and grow your business. Our Concierge Program will deliver marketing solutions tailored to your teamʼs unique needs. To participate in this program, Select partners should contact [partner marketing](mailto:Partner-Marketing@gitlab.com). 

### **Marketing Development Funds**

Select partners may be eligible for [Marketing Development Funds, or MDF,](https://about.gitlab.com/handbook/resellers/Channel-Program-Guide/MDF/) to acquire new GitLab customers, drive demand within an existing mutual customer base, or help educate GitLab teams. GitLab MDF can support multiple marketing efforts, including participation in trade shows, sales incentives, direct mailing costs, training, and more. 

#### Requesting Marketing Development Funds
Our Select partners are eligible to submit requests for consideration for marketing development funds (MDF). To learn more, check out our [MDF handbook page](https://about.gitlab.com/handbook/resellers/Channel-Program-Guide/MDF/) page or login to the [partner portal](https://partners.gitlab.com/prm/English/c/marketing) marketing development funds page.

### **GitLab Branded Item Requests**
Please submit your request for GitLab-branded items through your Channel Account Manager (CAM). We have been known to co-fund co-branded or locally produced swag.

## **GitLab Partner News and Updates**
There are a few ways we keep partners up to speed on GitLab initiatives.

### **Channel Webcasts**
We offer our partners webcasts to cover topics in greater depth. We encourage you to register for these webcasts whenever you are notified. 

### **Partner Flash**
Our premiere news-on-demand newsletter "Partner Flash" is designed to give partners all the key sales and marketing news. You decide how much or how little information you want to receive and how frequently. To receive the newsletter simply register for the partner portal and you'll receive this partner newsletter. 

### **The GitLab Handbook**
The GitLab Handbook is the central repository for how we run the company. As part of our value of being transparent, our Handbook is open to the world, and we welcome feedback. Although the Partner Portal is your first source of partner information, we often link to the Handbook for detailed information. If you are unable to find information you need on the Partner Portal, you are encouraged to search pages of the [GitLab Handbook](https://about.gitlab.com/handbook/).

## **More Channel Partner Program Information**
If you are looking for additional information on the GitLab Partner program see the following handbook pages.
* [Channel Partner Program Overview](https://about.gitlab.com/handbook/resellers/) 
* [Channel Program Guide](https://about.gitlab.com/handbook/resellers/Channel-Program-Guide/)
* [Channel Services Program](/handbook/resellers/services/)
* [Channel Services Catalog](/handbook/resellers/services/services-catalog/)
* [Channel Partner Training and Certifications](/handbook/resellers/training/)
* [Channel Partners: Working with GitLab](/handbook/resellers/channel-working-with-GitLab/)
* [Channel Tools and Resources](/handbook/resellers/channel-tools-resources/)
* [Alliance Program](https://about.gitlab.com/partners/technology-partners/)
 
## **Contact Us**
All authorized GitLab resellers are invited to the GitLab #resellers Slack channel. This Slack channel allows you to reach out to our sales and marketing team in a timely manner, as well as other resellers. Additionally, you can reach the GitLab Channel Team at partnersupport@gitlab.com and the GitLab marketing team at partner-marketing@gitlab.com.
