---
layout: handbook-page-toc
title: FY22-Q2 L&D Mental Health Newsletter
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mental Health Newsletter, Edition 3!

Hello, GitLab Team! Welcome to 3rd edition of the GitLab Mental Health newsletter! If you're interested in learning more about the intentions of this newsletter, check out our [goals](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/#long-term-goals) listed in the handbook.

And, if this is your first time reading the Mental Health Newsletter, be sure to check out [past editions](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/#past-newsletters) for great information about team member resources to prevent burnout and support mental wellbeing.


## Leadership feature

Did you know that L&D hosts a [CEO handbook learning session](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions) with Sid on a weekly basis? During these calls, we invite team members to join small-group conversations that dive deep into specific topics in our leadership handbook, or topics that are relevant to team member success.

During a recent session, the L&D team and other mental health advocates at GitLab talked to Sid about his experience with returning to work after time off, managing burnout, and addressing imposter syndrome. Check out the meeting recording below or on the [mental health handbook page](/company/culture/all-remote/mental-health/#introduction).

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/od_KdZqc69k" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


## Live Speaker Series about Rest Ethic

The L&D team was happy to welcome John Fitch back to speak to GitLab for a second Live Speaker series! We [first hosted John back in December 2020](/company/culture/all-remote/mental-health/#rest-and-time-off-are-productive) where we discussed the importance of taking time off.

During this second visit to GitLab, John spoke to manager enablement of time off and the ways that rest impacts both or productivity and engagement at work.

We were lucky to host John for 2 separate sessions. You can find both recordings on the [mental health handbook page](/company/culture/all-remote/mental-health/#your-rest-ethic-is-as-important-as-your-work-ethic). Here's the recording from our second session!

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/acVRU5UjJEo?start=04" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Want to hear from more speakers about mental wellness?

If you have a suggestion for, or a connection to, a speaker you'd like to see come to GitLab and speak about mental health, time off, resilience, or other topics around team member wellbeing, please tag `@slee24` on the [discussion issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/275) for this newsletter.

## GitLab Resource Feature

[Modern Health](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/#what-does-modern-health-offer) includes multiple benefits for team members. Here's a screenshot of the handbook from 2021-07-07 to highlight the current offerings:

![modern-health.png](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q2/modern-health.png)

We're using this newsletter to highlight [Modern Health Circles](https://circles.modernhealth.com/). Circles are live community sessions designed to help you proactively improve your mental health.

**Access to circles is unlimited!** So, take a second and check out their current circles to see what might feel like a good fit for your needs and interests. If you'd like, consider asking a colleague to attend a Circle with you, or share which Circles you might attend in your team Slack channel.

### Featured Modern Health Circles (as of 2021-07-07)

- [Foundations of Mental Health](https://circles.modernhealth.com/series/foundations)
- [Honoring LGBTQ+ Voices](https://circles.modernhealth.com/series/honoring-lgbtq-voices)
- [Black Lives Matter](https://circles.modernhealth.com/series/black-lives-matter)
- [Healing Asian Communities](https://circles.modernhealth.com/series/healing-asian-communities)
- [Covid-19 Resilience and Moving Forward](https://circles.modernhealth.com/series/covid-19-resilience-moving-forward)
- [Coping with the Covid Crisis in India](https://circles.modernhealth.com/series/coping-with-covid-india)
- [Healing Latinx/Hispanic Communities](https://circles.modernhealth.com/series/healing-hispanic-communities)


## Learning opportunities to improve team member wellbeing

Having [crucial conversations](/handbook/leadership/crucial-conversations/) both in and outside of work have a massive impact on our mental health.

In May 2021, the University of Michigan hosted a crucial conversation about mental health where they discussed ["prioritizing mental health, educating and advocating for resources and policies, and creating an uplifting environment for everyone."](https://publicengagement.umich.edu/crucial-conversations-mental-health-awareness/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/L8SOfJZ6tjY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Having open, safe conversations like this can be challenging and nerve wracking. Stigmas around mental health make it hard to bring up with our managers, colleagues, friends, and family. Equipping yourself with tools and strategies to have effective and safe crucial conversations can improve the discussions we have at GitLab about mental health and team member wellbeing.

The L&D team is certified to deliver the [Crucial Conversations](https://www.vitalsmarts.com/crucial-conversations-training/) training for interested team members. Read more about the program in this [crucial conversations blog post](https://about.gitlab.com/blog/2021/02/18/crucial-conversations/). If you're interested in the training, we encourage you to [fill out the interest form](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#crucial-conversations-training) to participate in the next internal training, happening in October.


## Discussion and Feedback

Do you have questions about mental health resources at GitLab? Or maybe you have a thought or resource that came up for you after reading this newsletter? Here's how to reach out:

| Question | Where to Learn More | Where to Ask a Question|
| ----- | ----- | ----- |
| GitLab PTO policy | [PTO Policy Handbook](/handbook/paid-time-off/) | [#people-connect Slack channel](https://app.slack.com/client/T02592416/C02360SQQFR/thread/C5P8T9VQX-1587584276.009700) |
| Learning opportunities about mental health and burnout management | [Mental Health Handbook Page](/company/culture/all-remote/mental-health/#introduction) | [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W/thread/CETG54GQ0-1609232817.392300) |
| Ideas, contrbutions, and feedback about this newsletter | [Newsletter handbook page](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter) | [Discussion Issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/275) |
